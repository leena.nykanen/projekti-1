Ensimmäinen kotitehtävä. 

cd komennolla valitaan kansio:
- cd tehtava1/ (valitaan alikansio siitä, missä nyt ollaan)
- cd ../ (mennään kansiotaso taaksepäin) 

mkdir komennolla luodaan uusi kansio:
- mkdir uusikansio 

lisäyksiä:

git init -komento luo uuden repositorion 
Gitlab antaa ohjeet, kuinka linkittää repositorio ja uusi projekti keskenään

git add . komennolla lisätään uudet muokkaukset jonoon
git commit -m "kommentti" komennolla saadaan tallennettua tehdyt muutokset //edited in dev branch 2
git push vie muutokset projektille gitlabiin (ennen tätä ne ovat vain omalla koneella)

git checkout -b nimi - luo uuden branchin (haaran)

//Lisää muokkauksia 
git checkout nimi - vaihtaa haaran

//git höpöhöpömastermuokkaus

